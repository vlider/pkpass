//
//  pkpass.m
//  pkpass
//
//  Created by Valerii Lider on 12/23/13.
//  Copyright (c) 2013 Spire. All rights reserved.
//

#import "pkpass.h"

#import <CommonCrypto/CommonDigest.h>
#include <stdio.h>
#include <stdint.h>

#include <string.h>

#include <openssl/crypto.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/x509_vfy.h>
#include <openssl/x509v3.h>

#include "apps.h"

#import "ZipFile.h"
#import "ZipWriteStream.h"
#import "FileInZipInfo.h"

static int save_certs(char *signerfile, STACK_OF(X509) *signers);
static int smime_cb(int ok, X509_STORE_CTX *ctx);

#define FileHashDefaultChunkSizeForReadingData 4096

#define SMIME_OP	0x10
#define SMIME_IP	0x20
#define SMIME_SIGNERS	0x40
#define SMIME_ENCRYPT	(1 | SMIME_OP)
#define SMIME_DECRYPT	(2 | SMIME_IP)
#define SMIME_SIGN	(3 | SMIME_OP | SMIME_SIGNERS)
#define SMIME_VERIFY	(4 | SMIME_IP)
#define SMIME_PK7OUT	(5 | SMIME_IP | SMIME_OP)
#define SMIME_RESIGN	(6 | SMIME_IP | SMIME_OP | SMIME_SIGNERS)

@interface NSString (sha1)
- (NSString *)fileSHA1;
@end

@implementation NSString (sha1)

- (NSString *)fileSHA1 {
    
    // Declare needed variables
    CFStringRef result = NULL;
    CFReadStreamRef readStream = NULL;
    
    // Get the file URL
    CFURLRef fileURL =
    CFURLCreateWithFileSystemPath(kCFAllocatorDefault,
                                  (CFStringRef)self,
                                  kCFURLPOSIXPathStyle,
                                  (Boolean)false);
    if (!fileURL) goto done;
    
    // Create and open the read stream
    readStream = CFReadStreamCreateWithFile(kCFAllocatorDefault,
                                            (CFURLRef)fileURL);
    if (!readStream) goto done;
    bool didSucceed = (bool)CFReadStreamOpen(readStream);
    if (!didSucceed) goto done;
    
    // Initialize the hash object
    CC_SHA1_CTX hashObject;
    CC_SHA1_Init(&hashObject);
    
    size_t chunkSizeForReadingData = FileHashDefaultChunkSizeForReadingData;
    
    // Feed the data to the hash object
    bool hasMoreData = true;
    while (hasMoreData) {
        uint8_t buffer[chunkSizeForReadingData];
        CFIndex readBytesCount = CFReadStreamRead(readStream,
                                                  (UInt8 *)buffer,
                                                  (CFIndex)sizeof(buffer));
        if (readBytesCount == -1) break;
        if (readBytesCount == 0) {
            hasMoreData = false;
            continue;
        }
        CC_SHA1_Update(&hashObject,
                       (const void *)buffer,
                       (CC_LONG)readBytesCount);
    }
    
    // Check if the read operation succeeded
    didSucceed = !hasMoreData;
    
    // Compute the hash digest
    unsigned char digest[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1_Final(digest, &hashObject);
    
    // Abort if the read operation failed
    if (!didSucceed) goto done;
    
    // Compute the string result
    char hash[2 * sizeof(digest) + 1];
    for (size_t i = 0; i < sizeof(digest); ++i) {
        snprintf(hash + (2 * i), 3, "%02x", (int)(digest[i]));
    }
    result = CFStringCreateWithCString(kCFAllocatorDefault,
                                       (const char *)hash,
                                       kCFStringEncodingUTF8);
    
done:
    
    if (readStream) {
        CFReadStreamClose(readStream);
        CFRelease(readStream);
    }
    if (fileURL) {
        CFRelease(fileURL);
    }
    return CFBridgingRelease(result);
}

@end

@interface pkpass (hidden)

@end

@implementation pkpass

- (NSString *)passWithCertificate:(NSString *)certificate
                  WWDRCertificate:(NSString *)wwdr
                       privateKey:(NSString *)pkey
               privateKeyPassword:(NSString *)password
                        resources:(NSArray *)resources
                    configuration:(NSDictionary *)configuration {
    
    NSParameterAssert(nil != certificate);
    NSParameterAssert(nil != pkey);
    NSParameterAssert(nil != wwdr);
    NSParameterAssert(nil != password);
    NSParameterAssert(nil != resources && 0 != resources.count);
    NSParameterAssert(nil != configuration);
    
    NSUUID *uuid = [NSUUID UUID];
    NSString *folderPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:uuid.UUIDString];
    
    NSMutableDictionary *manifest = [@{} mutableCopy];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    
    if (![fileManager createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:&error])
        return nil;
    
    for (NSString *path in resources) {
        
        NSString *destinationPath = [folderPath stringByAppendingPathComponent:path.lastPathComponent];
        if (![fileManager copyItemAtPath:path toPath:destinationPath error:&error])
            break;
        
        NSString *name = path.lastPathComponent;
        NSString *sha1 = destinationPath.fileSHA1;
        manifest[name] = sha1;
    }
    
    if (error) {
        
        [fileManager removeItemAtPath:folderPath error:&error];
        
        return nil;
    }
    
    NSString *path = [folderPath stringByAppendingPathComponent:@"pass.json"];
    [[NSJSONSerialization dataWithJSONObject:configuration options:NSJSONWritingPrettyPrinted error:&error] writeToFile:path atomically:YES];
    
    NSString *sha1 = path.fileSHA1;
    manifest[path.lastPathComponent] = sha1;
    
    if (error) {
        
        [fileManager removeItemAtPath:folderPath error:&error];
        
        return nil;
    }
    
    path = [folderPath stringByAppendingPathComponent:@"manifest.json"];
    [[NSJSONSerialization dataWithJSONObject:manifest options:NSJSONWritingPrettyPrinted error:NULL] writeToFile:path atomically:YES];
    
    
    int flags = PKCS7_DETACHED | PKCS7_BINARY;
    int operation = SMIME_SIGN;
    char *certfile = (char *)wwdr.UTF8String;
    char *signerfile = (char *)certificate.UTF8String;
    char *keyfile = (char *)pkey.UTF8String;
    char *infile = (char *)path.UTF8String;
    char *outfile = (char *)[folderPath stringByAppendingPathComponent:@"signature"].UTF8String;
    char *passargin = (char *)[NSString stringWithFormat:@"pass:%@", password].UTF8String;
    
    ENGINE *e = NULL;
	int ret = 0;
	const char *inmode = "r", *outmode = "w";
	char *recipfile = NULL;
	STACK_OF(OPENSSL_STRING) *sksigners = NULL, *skkeys = NULL;
	char *contfile=NULL;
	const EVP_CIPHER *cipher = NULL;
	PKCS7 *p7 = NULL;
	X509_STORE *store = NULL;
	X509 *cert = NULL, *recip = NULL, *signer = NULL;
	EVP_PKEY *key = NULL;
	STACK_OF(X509) *encerts = NULL, *other = NULL;
	BIO *in = NULL, *out = NULL, *indata = NULL;
	int badarg = 0;
	char *to = NULL, *from = NULL, *subject = NULL;
	char *CAfile = NULL, *CApath = NULL;
	char *passin = NULL;
	char *inrand = NULL;
	int need_rand = 0;
	int indef = 0;
	const EVP_MD *sign_md = NULL;
	int informat = FORMAT_SMIME, outformat = FORMAT_SMIME;
    int keyform = FORMAT_PEM;
#ifndef OPENSSL_NO_ENGINE
	char *engine=NULL;
#endif
    
    outformat = str2fmt("DER");
	X509_VERIFY_PARAM *vpm = NULL;
    
	ret = 1;
    
	apps_startup();
    
	if (bio_err == NULL)
    {
		if ((bio_err = BIO_new(BIO_s_file())) != NULL)
			BIO_set_fp(bio_err, stderr, BIO_NOCLOSE|BIO_FP_TEXT);
    }
    
	if (!load_config(bio_err, NULL))
		goto end;
    
	if (!(operation & SMIME_SIGNERS) && (skkeys || sksigners))
    {
		BIO_puts(bio_err, "Multiple signers or keys not allowed\n");
		goto argerr;
    }
    
	if (operation & SMIME_SIGNERS)
    {
		/* Check to see if any final signer needs to be appended */
		if (keyfile && !signerfile)
        {
			BIO_puts(bio_err, "Illegal -inkey without -signer\n");
			goto argerr;
        }
		if (signerfile)
        {
			if (!sksigners)
				sksigners = sk_OPENSSL_STRING_new_null();
			sk_OPENSSL_STRING_push(sksigners, signerfile);
			if (!skkeys)
				skkeys = sk_OPENSSL_STRING_new_null();
			if (!keyfile)
				keyfile = signerfile;
			sk_OPENSSL_STRING_push(skkeys, keyfile);
        }
		if (!sksigners)
        {
			BIO_printf(bio_err, "No signer certificate specified\n");
			badarg = 1;
        }
		signerfile = NULL;
		keyfile = NULL;
		need_rand = 1;
    }
    
	if (badarg)
    {
    argerr:
		BIO_printf (bio_err, "Usage smime [options] cert.pem ...\n");
		BIO_printf (bio_err, "where options are\n");
		BIO_printf (bio_err, "-encrypt       encrypt message\n");
		BIO_printf (bio_err, "-decrypt       decrypt encrypted message\n");
		BIO_printf (bio_err, "-sign          sign message\n");
		BIO_printf (bio_err, "-verify        verify signed message\n");
		BIO_printf (bio_err, "-pk7out        output PKCS#7 structure\n");
#ifndef OPENSSL_NO_DES
		BIO_printf (bio_err, "-des3          encrypt with triple DES\n");
		BIO_printf (bio_err, "-des           encrypt with DES\n");
#endif
#ifndef OPENSSL_NO_SEED
		BIO_printf (bio_err, "-seed          encrypt with SEED\n");
#endif
#ifndef OPENSSL_NO_RC2
		BIO_printf (bio_err, "-rc2-40        encrypt with RC2-40 (default)\n");
		BIO_printf (bio_err, "-rc2-64        encrypt with RC2-64\n");
		BIO_printf (bio_err, "-rc2-128       encrypt with RC2-128\n");
#endif
#ifndef OPENSSL_NO_AES
		BIO_printf (bio_err, "-aes128, -aes192, -aes256\n");
		BIO_printf (bio_err, "               encrypt PEM output with cbc aes\n");
#endif
#ifndef OPENSSL_NO_CAMELLIA
		BIO_printf (bio_err, "-camellia128, -camellia192, -camellia256\n");
		BIO_printf (bio_err, "               encrypt PEM output with cbc camellia\n");
#endif
		BIO_printf (bio_err, "-nointern      don't search certificates in message for signer\n");
		BIO_printf (bio_err, "-nosigs        don't verify message signature\n");
		BIO_printf (bio_err, "-noverify      don't verify signers certificate\n");
		BIO_printf (bio_err, "-nocerts       don't include signers certificate when signing\n");
		BIO_printf (bio_err, "-nodetach      use opaque signing\n");
		BIO_printf (bio_err, "-noattr        don't include any signed attributes\n");
		BIO_printf (bio_err, "-binary        don't translate message to text\n");
		BIO_printf (bio_err, "-certfile file other certificates file\n");
		BIO_printf (bio_err, "-signer file   signer certificate file\n");
		BIO_printf (bio_err, "-recip  file   recipient certificate file for decryption\n");
		BIO_printf (bio_err, "-in file       input file\n");
		BIO_printf (bio_err, "-inform arg    input format SMIME (default), PEM or DER\n");
		BIO_printf (bio_err, "-inkey file    input private key (if not signer or recipient)\n");
		BIO_printf (bio_err, "-keyform arg   input private key format (PEM or ENGINE)\n");
		BIO_printf (bio_err, "-out file      output file\n");
		BIO_printf (bio_err, "-outform arg   output format SMIME (default), PEM or DER\n");
		BIO_printf (bio_err, "-content file  supply or override content for detached signature\n");
		BIO_printf (bio_err, "-to addr       to address\n");
		BIO_printf (bio_err, "-from ad       from address\n");
		BIO_printf (bio_err, "-subject s     subject\n");
		BIO_printf (bio_err, "-text          include or delete text MIME headers\n");
		BIO_printf (bio_err, "-CApath dir    trusted certificates directory\n");
		BIO_printf (bio_err, "-CAfile file   trusted certificates file\n");
		BIO_printf (bio_err, "-crl_check     check revocation status of signer's certificate using CRLs\n");
		BIO_printf (bio_err, "-crl_check_all check revocation status of signer's certificate chain using CRLs\n");
#ifndef OPENSSL_NO_ENGINE
		BIO_printf (bio_err, "-engine e      use engine e, possibly a hardware device.\n");
#endif
		BIO_printf (bio_err, "-passin arg    input file pass phrase source\n");
		BIO_printf(bio_err,  "-rand file%cfile%c...\n", LIST_SEPARATOR_CHAR, LIST_SEPARATOR_CHAR);
		BIO_printf(bio_err,  "               load the file (or the files in the directory) into\n");
		BIO_printf(bio_err,  "               the random number generator\n");
		BIO_printf (bio_err, "cert.pem       recipient certificate(s) for encryption\n");
		goto end;
    }
    
#ifndef OPENSSL_NO_ENGINE
    e = setup_engine(bio_err, engine, 0);
#endif
    
	if (!app_passwd(bio_err, passargin, NULL, &passin, NULL))
    {
		BIO_printf(bio_err, "Error getting password\n");
		goto end;
    }
    
	if (need_rand)
    {
		app_RAND_load_file(NULL, bio_err, (inrand != NULL));
		if (inrand != NULL)
			BIO_printf(bio_err,"%ld semi-random bytes loaded\n",
                       app_RAND_load_files(inrand));
    }
    
	ret = 2;
    
	if (!(operation & SMIME_SIGNERS))
		flags &= ~PKCS7_DETACHED;
    
	if (operation & SMIME_OP)
    {
		if (outformat == FORMAT_ASN1)
			outmode = "wb";
    }
	else
    {
		if (flags & PKCS7_BINARY)
			outmode = "wb";
    }
    
	if (operation & SMIME_IP)
    {
		if (informat == FORMAT_ASN1)
			inmode = "rb";
    }
	else
    {
		if (flags & PKCS7_BINARY)
			inmode = "rb";
    }
    
	if (certfile)
    {
		if (!(other = load_certs(bio_err,certfile,FORMAT_PEM, NULL,
                                 e, "certificate file")))
        {
			ERR_print_errors(bio_err);
			goto end;
        }
    }
    
	if (recipfile && (operation == SMIME_DECRYPT))
    {
		if (!(recip = load_cert(bio_err,recipfile,FORMAT_PEM,NULL,
                                e, "recipient certificate file")))
        {
			ERR_print_errors(bio_err);
			goto end;
        }
    }
    
	if (operation == SMIME_DECRYPT)
    {
		if (!keyfile)
			keyfile = recipfile;
    }
	else if (operation == SMIME_SIGN)
    {
		if (!keyfile)
			keyfile = signerfile;
    }
	else keyfile = NULL;
    
	if (keyfile)
    {
		key = load_key(bio_err, keyfile, keyform, 0, passin, e,
                       "signing key file");
		if (!key)
			goto end;
    }
    
	if (infile)
    {
		if (!(in = BIO_new_file(infile, inmode)))
        {
			BIO_printf (bio_err,
                        "Can't open input file %s\n", infile);
			goto end;
        }
    }
	else
		in = BIO_new_fp(stdin, BIO_NOCLOSE);
    
	if (operation & SMIME_IP)
    {
		if (informat == FORMAT_SMIME)
			p7 = SMIME_read_PKCS7(in, &indata);
		else if (informat == FORMAT_PEM)
			p7 = PEM_read_bio_PKCS7(in, NULL, NULL, NULL);
		else if (informat == FORMAT_ASN1)
			p7 = d2i_PKCS7_bio(in, NULL);
		else
        {
			BIO_printf(bio_err, "Bad input format for PKCS#7 file\n");
			goto end;
        }
        
		if (!p7)
        {
			BIO_printf(bio_err, "Error reading S/MIME message\n");
			goto end;
        }
		if (contfile)
        {
			BIO_free(indata);
			if (!(indata = BIO_new_file(contfile, "rb")))
            {
				BIO_printf(bio_err, "Can't read content file %s\n", contfile);
				goto end;
            }
        }
    }
    
	if (outfile)
    {
		if (!(out = BIO_new_file(outfile, outmode)))
        {
			BIO_printf (bio_err,
                        "Can't open output file %s\n", outfile);
			goto end;
        }
    }
	else
    {
		out = BIO_new_fp(stdout, BIO_NOCLOSE);
#ifdef OPENSSL_SYS_VMS
		{
		    BIO *tmpbio = BIO_new(BIO_f_linebuffer());
		    out = BIO_push(tmpbio, out);
		}
#endif
    }
    
	if (operation == SMIME_VERIFY)
    {
		if (!(store = setup_verify(bio_err, CAfile, CApath)))
			goto end;
		X509_STORE_set_verify_cb(store, smime_cb);
		if (vpm)
			X509_STORE_set1_param(store, vpm);
    }
    
    
	ret = 3;
    
	if (operation == SMIME_ENCRYPT)
    {
		if (indef)
			flags |= PKCS7_STREAM;
		p7 = PKCS7_encrypt(encerts, in, cipher, flags);
    }
	else if (operation & SMIME_SIGNERS)
    {
		int i;
		/* If detached data content we only enable streaming if
		 * S/MIME output format.
		 */
		if (operation == SMIME_SIGN)
        {
			if (flags & PKCS7_DETACHED)
            {
				if (outformat == FORMAT_SMIME)
					flags |= PKCS7_STREAM;
            }
			else if (indef)
				flags |= PKCS7_STREAM;
			flags |= PKCS7_PARTIAL;
			p7 = PKCS7_sign(NULL, NULL, other, in, flags);
			if (!p7)
				goto end;
        }
		else
			flags |= PKCS7_REUSE_DIGEST;
		for (i = 0; i < sk_OPENSSL_STRING_num(sksigners); i++)
        {
			signerfile = sk_OPENSSL_STRING_value(sksigners, i);
			keyfile = sk_OPENSSL_STRING_value(skkeys, i);
			signer = load_cert(bio_err, signerfile,FORMAT_PEM, NULL,
                               e, "signer certificate");
			if (!signer)
				goto end;
			key = load_key(bio_err, keyfile, keyform, 0, passin, e,
                           "signing key file");
			if (!key)
				goto end;
			if (!PKCS7_sign_add_signer(p7, signer, key,
                                       sign_md, flags))
				goto end;
			X509_free(signer);
			signer = NULL;
			EVP_PKEY_free(key);
			key = NULL;
        }
		/* If not streaming or resigning finalize structure */
		if ((operation == SMIME_SIGN) && !(flags & PKCS7_STREAM))
        {
			if (!PKCS7_final(p7, in, flags))
				goto end;
        }
    }
    
	if (!p7)
    {
		BIO_printf(bio_err, "Error creating PKCS#7 structure\n");
		goto end;
    }
    
	ret = 4;
	if (operation == SMIME_DECRYPT)
    {
		if (!PKCS7_decrypt(p7, key, recip, out, flags))
        {
			BIO_printf(bio_err, "Error decrypting PKCS#7 structure\n");
			goto end;
        }
    }
	else if (operation == SMIME_VERIFY)
    {
		STACK_OF(X509) *signers;
		if (PKCS7_verify(p7, other, store, indata, out, flags))
			BIO_printf(bio_err, "Verification successful\n");
		else
        {
			BIO_printf(bio_err, "Verification failure\n");
			goto end;
        }
		signers = PKCS7_get0_signers(p7, other, flags);
		if (!save_certs(signerfile, signers))
        {
			BIO_printf(bio_err, "Error writing signers to %s\n",
                       signerfile);
			ret = 5;
			goto end;
        }
		sk_X509_free(signers);
    }
	else if (operation == SMIME_PK7OUT)
		PEM_write_bio_PKCS7(out, p7);
	else
    {
		if (to)
			BIO_printf(out, "To: %s\n", to);
		if (from)
			BIO_printf(out, "From: %s\n", from);
		if (subject)
			BIO_printf(out, "Subject: %s\n", subject);
		if (outformat == FORMAT_SMIME)
        {
			if (operation == SMIME_RESIGN)
				SMIME_write_PKCS7(out, p7, indata, flags);
			else
				SMIME_write_PKCS7(out, p7, in, flags);
        }
		else if (outformat == FORMAT_PEM)
			PEM_write_bio_PKCS7_stream(out, p7, in, flags);
		else if (outformat == FORMAT_ASN1)
			i2d_PKCS7_bio_stream(out,p7, in, flags);
		else
        {
			BIO_printf(bio_err, "Bad output format for PKCS#7 file\n");
			goto end;
        }
    }
	ret = 0;
end:
	if (need_rand)
		app_RAND_write_file(NULL, bio_err);
	if (ret) ERR_print_errors(bio_err);
	sk_X509_pop_free(encerts, X509_free);
	sk_X509_pop_free(other, X509_free);
	if (vpm)
		X509_VERIFY_PARAM_free(vpm);
	if (sksigners)
		sk_OPENSSL_STRING_free(sksigners);
	if (skkeys)
		sk_OPENSSL_STRING_free(skkeys);
	X509_STORE_free(store);
	X509_free(cert);
	X509_free(recip);
	X509_free(signer);
	EVP_PKEY_free(key);
	PKCS7_free(p7);
	BIO_free(in);
	BIO_free(indata);
	BIO_free_all(out);
	if (passin) OPENSSL_free(passin);
    
    NSString *result = nil;
    
    if ([fileManager fileExistsAtPath:[NSString stringWithUTF8String:outfile]]) {
        
        result = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]
                  stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pkpass", uuid.UUIDString]];
        
        ZipFile *zipFile = [[ZipFile alloc] initWithFileName:result mode:ZipFileModeCreate];
        
        for (NSString *file in [fileManager contentsOfDirectoryAtPath:folderPath error:&error]) {
            
            ZipWriteStream *stream = [zipFile writeFileInZipWithName:file
                                                    compressionLevel:ZipCompressionLevelBest];
            
            [stream writeData:[NSData dataWithContentsOfFile:[folderPath stringByAppendingPathComponent:file]]];
            [stream finishedWriting];
        }
        
        [zipFile close];
        
        if (![fileManager fileExistsAtPath:result]) {
            
            result = nil;
        }
    }
    
    [fileManager removeItemAtPath:folderPath error:NULL];
    
    return result;
}

@end

static int save_certs(char *signerfile, STACK_OF(X509) *signers)
{
	int i;
	BIO *tmp;
	if (!signerfile)
		return 1;
	tmp = BIO_new_file(signerfile, "w");
	if (!tmp) return 0;
	for(i = 0; i < sk_X509_num(signers); i++)
		PEM_write_bio_X509(tmp, sk_X509_value(signers, i));
	BIO_free(tmp);
	return 1;
}


/* Minimal callback just to output policy info (if any) */

static int smime_cb(int ok, X509_STORE_CTX *ctx)
{
	int error;
    
	error = X509_STORE_CTX_get_error(ctx);
    
	if ((error != X509_V_ERR_NO_EXPLICIT_POLICY)
		&& ((error != X509_V_OK) || (ok != 2)))
		return ok;
    
	policies_print(NULL, ctx);
    
	return ok;
    
}
