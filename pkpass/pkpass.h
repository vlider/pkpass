//
//  pkpass.h
//  pkpass
//
//  Created by Valerii Lider on 12/23/13.
//  Copyright (c) 2013 Spire. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface pkpass : NSObject

/*
 * builds pkpass file from given certificates and pass options
 * certificate - pass type id in pem format
 * key - private key in pem format
 * wwdr - Apple worldwide Developer Relations certificate in pem format
 * password - password for opening private key file
 * resources - array with paths to resources, like logo, icon, etc. No needs to copy into specific folder, just list all used files.
 * configuration - dictionary with pass settings
 */
- (NSString *)passWithCertificate:(NSString *)certificate
                  WWDRCertificate:(NSString *)wwdr
                       privateKey:(NSString *)key
               privateKeyPassword:(NSString *)password
                        resources:(NSArray *)resources
                    configuration:(NSDictionary *)configuration;
@end
